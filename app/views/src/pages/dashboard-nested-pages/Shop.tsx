import React from "react";
import CreateShop from "../../components/Inc/CreateShopForm";

const Shop = () => (
    <div className="shop-container">
        <h1>This is Shop SubPage</h1>
        <CreateShop/>
    </div>
);

export default Shop;